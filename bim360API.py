import requests
import re
from download_manager import DownloadManager


def get_token(id, secret, scope):
    """ BIM 360 2-Leg Authorization Returns Access Token

        arguments:
        id -- client id string
        secret -- client secret string
        scope -- read, write, create --> https://forge.autodesk.com/en/docs/oauth/v2/developers_guide/scopes/

    """
    auth_url = r'https://developer.api.autodesk.com/authentication/v1/authenticate'

    headers = { 'Content-Type': 'application/x-www-form-urlencoded', 'user-agent':'dkfjekrjekrjeikrjekrejrk'}
    payload = {
        'client_id' : f'{id}',
        'client_secret': f'{secret}',
        'grant_type' : 'client_credentials',
        'scope': f'{scope}'
    }

    response = requests.post(auth_url, data=payload, headers=headers)
    data = response.json()
    return data['access_token']

def get_hub_ids(token):
    """Get BIM 360 project hubs"""
    headers = {'Authorization': f'Bearer {token}'}
    response = requests.get('https://developer.api.autodesk.com/project/v1/hubs', headers=headers)
    data = response.json()['data']
    hub_ids = []
    for hub in data:
        hub_ids.append(hub['id'])
    return hub_ids

def get_projects(token, hub_id):
    """Get BIM 360 projects"""
    headers = {'Authorization': f'Bearer {token}'}
    response = requests.get(f'https://developer.api.autodesk.com/project/v1/hubs/{hub_id}/projects', headers=headers)
    return response.json()['data']

def get_project_contents(token, projects, filter_project_name = None):
    """ Get the latest BIM360 resource urls
        
        arguments
        token -- auth token string
        projects -- JSON projects data from BIM360
        filter_project_name -- String for filterinng a known project name

    """
    headers = {'Authorization': f'Bearer {token}'}

    if filter_project_name:
        # use a generator to make the search a bit faster
        project = next(item for item in projects if item['attributes']['name'] == filter_project_name)
        project_id = project['id']
        root_folder_id = project['relationships']['rootFolder']['data']['id']
        response = requests.get(f'https://developer.api.autodesk.com/data/v1/projects/{project_id}/folders/{root_folder_id}/contents', headers=headers)
        return response.json()

    else:
        for project in projects:
            project_id = project['id']
            root_folder_id = project['relationships']['rootFolder']['data']['id']
            response = requests.get(f'https://developer.api.autodesk.com/data/v1/projects/{project_id}/folders/{root_folder_id}/contents', headers=headers)
            return response.json()

def get_folder_contents(token, contents, folder_name, filetype_filter=None):
    """ Get a folder name and return the contents

        arguments
        token = Access token string
        contents = Array of folder contents from get_project_contents return
        folder_name = Folder name string
        filetype_fileter = Optional filetype extension filter 'string. i.e. "rvt"
    
    """
    folder_contents = next(item for item in contents['data'] if item['attributes']['displayName'] == folder_name)
    url = folder_contents['links']['self']['href']
    headers = {'Authorization': f'Bearer {token}'}
    response = requests.get(f'{url}/contents', headers=headers)
    data = response.json()
    if filetype_filter and data['included']:
        files = [file for file in data['included'] if file['attributes']['fileType'] == filetype_filter]
        return files
    else:
        content = response.json()
        return content
            
def get_project_number_from_rvt_file(filename):
    """ extracts the project number from our filenaming convention for Revit files
    
        arguments
        filename -- the filename string must contain a 5 digit format project number
    
    """
    try:
         project_number = re.findall('\d+', filename)[0]
         if len(project_number) == 5:
            return project_number
         else:
            raise SyntaxError(f"{project_number}: is not a valid 5 digit number")
    except SyntaxError as e:
        print(f"{e} - Project number was not found in {filename}")
        return None

def build_resource_data(project_number, project_name, file_url, rvt_filename):
    """Builds a dict of resources to send to a download manager
    
        arguments
        project_number -- a string 6 digit project number
    """
    resource_data = {'project_number':project_number, 'project_name':project_name,'link_address':file_url, 'link_name': rvt_filename}
    return resource_data         

# get the token for the app authentication
ACCESS_TOKEN = get_token(
    id = 'X9W2ECaMNPQi3x9Io4x1RhTlkwANY9Qe', 
    secret = '8g8xeOZJOtJIGMSf',
    scope = 'data:read' 
)