import os

from flask import Flask

from blueprints.home.home import home_bp
from blueprints.projects.projects import projects_bp
from blueprints.files.files import files_bp
from blueprints.settings.settings import settings_bp
from blueprints.errors.errors import errors_bp

# entry point factory
def create_app(test_config=None):
    # create and config the app
    app = Flask(__name__, instance_relative_config=True)
    app.config.from_mapping(
        SECRET_KEY='dev',
        DATABASE = os.path.join(app.instance_path, 'funnel.sqlite')
    )

    if test_config is None:
        # load the instance config, if it exists, when not testing
        app.config.from_pyfile('config.py', silent=True)
    else:
        # load the text config if passed in
        app.config.from_mapping(test_config)

    # ensure the instance folder exists
    try:
        os.makedirs(app.instance_path)
    except OSError:
        pass

    # register blueprints
    app.register_blueprint(home_bp)
    app.register_blueprint(projects_bp)
    app.register_blueprint(files_bp)
    app.register_blueprint(settings_bp)
    app.register_blueprint(errors_bp)
    
    return app