from flask import Blueprint, render_template

files_bp = Blueprint('files', __name__, template_folder='templates')

@files_bp.route('/files')
def files():
    return render_template('files.html')
