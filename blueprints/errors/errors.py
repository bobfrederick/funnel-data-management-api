from flask import Blueprint, render_template

errors_bp = Blueprint('errors',__name__,
    template_folder='templates')

@errors_bp.route('/errors')
def errors():
    return render_template('errors.html')