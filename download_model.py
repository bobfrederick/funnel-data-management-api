import os
import datetime
from bim360API import ACCESS_TOKEN, get_hub_ids, get_projects, get_project_contents, get_folder_contents
from download_manager import  DownloadManager

# get the hub id
hubs = get_hub_ids(ACCESS_TOKEN)

# get the projects
projects = get_projects(ACCESS_TOKEN, hubs[0])

#get the contents of the project folders
contents = get_project_contents(ACCESS_TOKEN, projects, "Stone Canyon")

#get the contents of the "Project Files Folder"
project_files_contents = get_folder_contents(ACCESS_TOKEN, contents, "Project Files")
#get the contents of the Arch  & Landscape Folder
arch_folder_files =  get_folder_contents(ACCESS_TOKEN,project_files_contents,"01 Architectural Model", "rvt")
landscape_folder_files = get_folder_contents(ACCESS_TOKEN, project_files_contents, "02 Landscape Model","rvt")


# test on multiple projects
project_list = ["Stone Canyon", "Echo Street West"]

project_content = []
for name in project_list:
    project_content.append(get_project_contents(ACCESS_TOKEN, projects, name))

project_files = []
for folder in project_content:
    project_files.append(get_folder_contents(folder, "Project Files", None))

# get a list of all the directory paths
dirpaths = [f.path for f in os.scandir("P:\\") if f.is_dir()]

download_list = []
# setup isoformat date folder name.
date = datetime.datetime.now().date().strftime("%Y%m%d")[2:]
new_foldername = f'{date}_BIM 360'


for item in urls:
    project_no = item['project_number']
    filename = item['link_name']
    url = item['link_address']
    for i, folder_name in enumerate(dirpaths):
        #find folder path in local P:/ drive using the project number
        if project_no in folder_name:
            new_filepath = f'{folder_name}\{project_no}_000 Models\{project_no}_000_3 Revit\{project_no}_02 Archived'
            # create a new BIM 360 folder
            new_directory = os.path.join(new_filepath,new_foldername)
            if os.path.exists(new_directory):
                output_filepath = os.path.join(new_directory,filename)
                if os.path.exists(output_filepath):
                    today = datetime.datetime.now().date().strftime("%a, %d %b  %Y")
                    print(f"This file was already archived on {today} will be skipped")
                else:
                    download_list.append((url, output_filepath, url_headers))
            else:
                os.makedirs(new_directory)
                output_filepath = os.path.join(new_directory,filename)
                download_list.append((url, output_filepath, url_headers))

#download_manager = DownloadManager(download_list, thread_count=4)
#download_manager.begin_downloads()