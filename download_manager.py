""" Import """
import sys
import os
import time
from threading import Thread
from queue import Queue
import argparse
import binascii
import urllib.request
import requests

class Downloader(Thread):
    """ Downloader class - reads queue and downloads each file in succession """
    def __init__(self, queue):
        Thread.__init__(self, name=binascii.hexlify(os.urandom(16)))
        self.queue = queue

    def run(self):
        while True:
            # gets the url from the queue
            download_list = self.queue.get()
            url = download_list[0]
            output_path = download_list[1]
            headers = download_list[2]
            # download the file
            print("* Thread {0} - processing URL".format(self.name))
            self.download_file(url, headers, output_path)
            # send a signal to the queue that the job is done
            self.queue.task_done()

    def download_file(self, url, headers, output_path):
        """ download file """
        t_start = time.clock()
        r = requests.get(url, headers = headers)
        if r.status_code == 200:
            t_elapsed = time.clock() - t_start
            print("* Thread: {0} Downloaded {1} in {2} seconds".format(self.name, url, str(t_elapsed)))
            with open(output_path, 'wb') as out:
                out.write(r.content)
        else:
            print("* Thread: {0} Bad URL: {1}".format(self.name, url))

class DownloadManager():
    """ Spawns dowloader threads and manages URL downloads queue 

        arguments:\n
        download_list -- a tuple containing 2 string params and a dict for header ( url, output_dir, url_headers)\n
        thread_count -- an int to specify number of threads to use   
    
    """
    def __init__(self, download_list, thread_count=4):
        self.thread_count = thread_count
        self.download_list = download_list

    def begin_downloads(self):
        """
        Start the downloader threads, fill the queue with the URLs and\n
        then feed the threads URLs via the queue
        """
        queue = Queue()
        # Create a thread pool and give them a queue
        for i in range(self.thread_count):
            t = Downloader(queue)
            t.setDaemon(True)
            t.start()
        # Load the queue from the download list
        for item in self.download_list:
            queue.put(item)
        # Wait for the queue to finish
        queue.join()
        return
